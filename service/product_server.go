package service

import (
	"context"
	"errors"
	"io"
	"log"

	"jwt_autentication2/pb"

	"github.com/google/uuid"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// ProductServer is the server that provides Product services
type ProductServer struct {
	pb.UnimplementedProductServiceServer
	ProductStore ProductStore
	ratingStore  RatingStore
}

// NewProductServer returns a new ProductServer
func NewProductServer(ProductStore ProductStore, ratingStore RatingStore) *ProductServer {
	return &ProductServer{
		ProductStore: ProductStore,
		ratingStore:  ratingStore,
	}
}

// CreateProduct is a unary RPC to create a new Product
func (server *ProductServer) CreateProduct(
	ctx context.Context,
	req *pb.CreateProductRequest,
) (*pb.CreateProductResponse, error) {
	Product := req.GetProduct()
	log.Printf("receive a create-Product request with id: %s", Product.Id)

	if len(Product.Id) > 0 {
		// check if it's a valid UUID
		_, err := uuid.Parse(Product.Id)
		if err != nil {
			return nil, status.Errorf(codes.InvalidArgument, "Product ID is not a valid UUID: %v", err)
		}
	} else {
		id, err := uuid.NewRandom()
		if err != nil {
			return nil, status.Errorf(codes.Internal, "cannot generate a new Product ID: %v", err)
		}
		Product.Id = id.String()
	}

	if err := contextError(ctx); err != nil {
		return nil, err
	}

	// save the Product to store
	err := server.ProductStore.Save(Product)
	if err != nil {
		code := codes.Internal
		if errors.Is(err, ErrAlreadyExists) {
			code = codes.AlreadyExists
		}

		return nil, status.Errorf(code, "cannot save Product to the store: %v", err)
	}

	log.Printf("saved Product with id: %s", Product.Id)

	res := &pb.CreateProductResponse{
		Id: Product.Id,
	}
	return res, nil
}

// RateProduct is a bidirectional-streaming RPC that allows client to rate a stream of Products
// with a score, and returns a stream of average score for each of them
func (server *ProductServer) RateProduct(stream pb.ProductService_RateProductServer) error {
	for {
		err := contextError(stream.Context())
		if err != nil {
			return err
		}

		req, err := stream.Recv()
		if err == io.EOF {
			log.Print("no more data")
			break
		}
		if err != nil {
			return logError(status.Errorf(codes.Unknown, "cannot receive stream request: %v", err))
		}

		ProductID := req.GetProductId()
		score := req.GetScore()

		log.Printf("received a rate-Product request: id = %s, score = %.2f", ProductID, score)

		rating, err := server.ratingStore.Add(ProductID, score)
		if err != nil {
			return logError(status.Errorf(codes.Internal, "cannot add rating to the store: %v", err))
		}

		res := &pb.RateProductResponse{
			ProductId:    ProductID,
			RatedCount:   rating.Count,
			AverageScore: rating.Sum / float64(rating.Count),
		}

		err = stream.Send(res)
		if err != nil {
			return logError(status.Errorf(codes.Unknown, "cannot send stream response: %v", err))
		}
	}

	return nil
}

func contextError(ctx context.Context) error {
	switch ctx.Err() {
	case context.Canceled:
		return logError(status.Error(codes.Canceled, "request is canceled"))
	case context.DeadlineExceeded:
		return logError(status.Error(codes.DeadlineExceeded, "deadline is exceeded"))
	default:
		return nil
	}
}

func logError(err error) error {
	if err != nil {
		log.Print(err)
	}
	return err
}
