package service

import (
	"errors"
	"fmt"
	"sync"

	"jwt_autentication2/pb"

	"github.com/jinzhu/copier"
)

// ErrAlreadyExists is returned when a record with the same ID already exists in the store
var ErrAlreadyExists = errors.New("record already exists")

// ProductStore is an interface to store Product
type ProductStore interface {
	// Save saves the Product to the store
	Save(Product *pb.Product) error
}

// InMemoryProductStore stores Product in memory
type InMemoryProductStore struct {
	mutex sync.RWMutex
	data  map[string]*pb.Product
}

// NewInMemoryProductStore returns a new InMemoryProductStore
func NewInMemoryProductStore() *InMemoryProductStore {
	return &InMemoryProductStore{
		data: make(map[string]*pb.Product),
	}
}

// Save saves the Product to the store
func (store *InMemoryProductStore) Save(Product *pb.Product) error {
	store.mutex.Lock()
	defer store.mutex.Unlock()

	if store.data[Product.Id] != nil {
		return ErrAlreadyExists
	}

	other, err := deepCopy(Product)
	if err != nil {
		return err
	}

	store.data[other.Id] = other
	return nil
}

func deepCopy(Product *pb.Product) (*pb.Product, error) {
	other := &pb.Product{}

	err := copier.Copy(other, Product)
	if err != nil {
		return nil, fmt.Errorf("cannot copy Product data: %w", err)
	}

	return other, nil
}
