package service

import "sync"

// RatingStore is an interface to store product ratings
type RatingStore interface {
	// Add adds a new product score to the store and returns its rating
	Add(productID string, score float64) (*Rating, error)
}

// Rating contains the rating information of a product
type Rating struct {
	Count uint32
	Sum   float64
}

// InMemoryRatingStore stores product ratings in memory
type InMemoryRatingStore struct {
	mutex  sync.RWMutex
	rating map[string]*Rating
}

// NewInMemoryRatingStore returns a new InMemoryRatingStore
func NewInMemoryRatingStore() *InMemoryRatingStore {
	return &InMemoryRatingStore{
		rating: make(map[string]*Rating),
	}
}

// Add adds a new product score to the store and returns its rating
func (store *InMemoryRatingStore) Add(productID string, score float64) (*Rating, error) {
	store.mutex.Lock()
	defer store.mutex.Unlock()

	rating := store.rating[productID]
	if rating == nil {
		rating = &Rating{
			Count: 1,
			Sum:   score,
		}
	} else {
		rating.Count++
		rating.Sum += score
	}

	store.rating[productID] = rating
	return rating, nil
}
