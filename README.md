# Jwt Authentication

This repository contains the Golang codes for jwt authentication using gRPC interceptors.

## gRPC Interceptor

gRPC Interceptor is like a middleware function that can be added on both server side and client side.

The two types of gRPC interceptor:

1. Unary Interceptor
2. Stream Interceptor

The gRPC interceptor is used as a middleware to process the client and server request and response and authorise and verify the JWT tokens.

This Repository demonstrates the JWT(Jason Web Token) authentication mechanism using gRPC interceptor:

It provides 2 GRPC API's:

1. Create a new Product: [unary gRPC]:

    This is a unary RPC API that creates a new Product.

    The input of the API is a Product object, and it returns the unique ID of the created Product.

    The Product ID is a UUID, and can be set by the client, or randomly generated by the server if it's not provided.

2. Rate multiple Products and get back average rating for each of them: [bidirectional-streaming gRPC]:

    This is a bidirectional-streaming RPC API that allows client to rate multiple laptops, each with a score between 1 to 10, and get back the average rating score for each of them.

    The input of the API is a stream of requests, each with a laptop ID and a score.

    The API will returns a stream of responses, each contains a laptop ID, the number of times that laptop was rated, and the average rated score.

## Setup development environment

- Install `protoc`:

```bash
brew install protobuf
```

- Install `protoc-gen-go` and `protoc-gen-go-grpc`

```bash
go get google.golang.org/protobuf/cmd/protoc-gen-go
go get google.golang.org/grpc/cmd/protoc-gen-go-grpc
```


## How to run

- Generate codes from proto files:

```bash
make gen
```

- Run server and client:

```bash
make server
make user-login / make admin-login
```