package client

import (
	"context"
	"fmt"
	"io"
	"log"
	"time"

	"jwt_autentication2/pb"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// ProductClient is a client to call Product service RPCs
type ProductClient struct {
	service pb.ProductServiceClient
}

// NewProductClient returns a new Product client
func NewProductClient(cc *grpc.ClientConn) *ProductClient {
	service := pb.NewProductServiceClient(cc)
	return &ProductClient{service}
}

// CreateProduct calls create Product RPC
func (ProductClient *ProductClient) CreateProduct(Product *pb.Product) {
	req := &pb.CreateProductRequest{
		Product: Product,
	}

	// set timeout
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	res, err := ProductClient.service.CreateProduct(ctx, req)
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.AlreadyExists {
			// not a big deal
			log.Print("Product already exists")
		} else {
			log.Fatal("cannot create Product: ", err)
		}
		return
	}

	log.Printf("created Product with id: %s", res.Id)
}

// RateProduct calls rate Product RPC
func (ProductClient *ProductClient) RateProduct(ProductIDs []string, scores []float64) error {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	stream, err := ProductClient.service.RateProduct(ctx)
	if err != nil {
		return fmt.Errorf("cannot rate Product: %v", err)
	}

	waitResponse := make(chan error)
	// go routine to receive responses
	go func() {
		for {
			res, err := stream.Recv()
			if err == io.EOF {
				log.Print("no more responses")
				waitResponse <- nil
				return
			}
			if err != nil {
				waitResponse <- fmt.Errorf("cannot receive stream response: %v", err)
				return
			}

			log.Print("received response: ", res)
		}
	}()

	// send requests
	for i, ProductID := range ProductIDs {
		req := &pb.RateProductRequest{
			ProductId: ProductID,
			Score:     scores[i],
		}

		err := stream.Send(req)
		if err != nil {
			return fmt.Errorf("cannot send stream request: %v - %v", err, stream.RecvMsg(nil))
		}

		log.Print("sent request: ", req)
	}

	err = stream.CloseSend()
	if err != nil {
		return fmt.Errorf("cannot close send: %v", err)
	}

	err = <-waitResponse
	return err
}
