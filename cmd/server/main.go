package main

import (
	"log"
	"net"
	"time"

	"jwt_autentication2/service"

	"jwt_autentication2/pb"

	"google.golang.org/grpc"
)

// seedUsers creates some default users to user store
func seedUsers(userStore service.UserStore) error {
	err := createUser(userStore, "admin1", "secret", "admin")
	if err != nil {
		return err
	}
	return createUser(userStore, "user1", "secret", "user")
}

// createUser stores some default users to user store
func createUser(userStore service.UserStore, username, password, role string) error {
	user, err := service.NewUser(username, password, role)
	if err != nil {
		return err
	}
	return userStore.Save(user)
}

const (
	secretKey     = "secret"
	tokenDuration = 15 * time.Minute
	port          = ":9001"
)

// accessibleRoles specifies authentication permissions
func accessibleRoles() map[string][]string {
	const ProductServicePath = "/jwt_authentication.ProductService/"

	return map[string][]string{
		ProductServicePath + "CreateProduct": {"admin"},
		ProductServicePath + "RateProduct":   {"admin", "user"},
	}
}

func main() {

	userStore := service.NewInMemoryUserStore()
	err := seedUsers(userStore)
	if err != nil {
		log.Fatal("cannot seed users: ", err)
	}

	jwtManager := service.NewJWTManager(secretKey, tokenDuration)
	authServer := service.NewAuthServer(userStore, jwtManager)

	listener, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatal("cannot start server: ", err)
	}

	ProductStore := service.NewInMemoryProductStore()
	ratingStore := service.NewInMemoryRatingStore()
	ProductServer := service.NewProductServer(ProductStore, ratingStore)

	interceptor := service.NewAuthInterceptor(jwtManager, accessibleRoles())
	serverOptions := []grpc.ServerOption{
		grpc.UnaryInterceptor(interceptor.Unary()),
		grpc.StreamInterceptor(interceptor.Stream()),
	}

	grpcServer := grpc.NewServer(serverOptions...)

	pb.RegisterAuthServiceServer(grpcServer, authServer)
	pb.RegisterProductServiceServer(grpcServer, ProductServer)

	log.Printf("Start GRPC server at %s", listener.Addr().String())
	err = grpcServer.Serve(listener)
	if err != nil {
		log.Fatal("cannot start server: ", err)
	}
}
