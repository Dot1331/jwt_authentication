package main

import (
	"flag"
	"fmt"
	"log"
	"strings"
	"time"

	"jwt_autentication2/client"
	"jwt_autentication2/sample"

	"google.golang.org/grpc"
)

const (
	refreshDuration = 30 * time.Second
	port            = ":9001"
)

// testRateProduct rates created product using CreateProduct and RateProduct RPC's
func testRateProduct(ProductClient *client.ProductClient) {
	n := 3
	ProductIDs := make([]string, n)

	for i := 0; i < n; i++ {
		Product := sample.NewProduct()
		ProductIDs[i] = Product.GetId()
		ProductClient.CreateProduct(Product)
	}

	scores := make([]float64, n)
	for {
		fmt.Print("rate Product (y/n)? ")
		var answer string
		fmt.Scan(&answer)

		if strings.ToLower(answer) != "y" {
			break
		}

		for i := 0; i < n; i++ {
			scores[i] = sample.RandomProductScore()
		}

		err := ProductClient.RateProduct(ProductIDs, scores)
		if err != nil {
			log.Fatal(err)
		}
	}
}

// authMethods specifies authentication permissions
func authMethods() map[string]bool {
	const ProductServicePath = "/jwt_authentication.ProductService/"

	return map[string]bool{
		ProductServicePath + "CreateProduct": true,
		ProductServicePath + "RateProduct":   true,
	}
}

func main() {
	username := flag.String("user", "user1", "username for authentication")
	password := flag.String("pass", "secret", "password for authentication")
	flag.Parse()
	log.Println("dial server:", port)

	grpcOptions := grpc.WithInsecure()

	cc1, err := grpc.Dial(port, grpcOptions)
	if err != nil {
		log.Fatal("cannot dial server: ", err)
	}

	authClient := client.NewAuthClient(cc1, *username, *password)
	interceptor, err := client.NewAuthInterceptor(authClient, authMethods(), refreshDuration)
	if err != nil {
		log.Fatal("cannot create auth interceptor: ", err)
	}

	cc2, err := grpc.Dial(
		port,
		grpcOptions,
		grpc.WithUnaryInterceptor(interceptor.Unary()),
		grpc.WithStreamInterceptor(interceptor.Stream()),
	)
	if err != nil {
		log.Fatal("cannot dial server: ", err)
	}

	ProductClient := client.NewProductClient(cc2)
	testRateProduct(ProductClient)

}
