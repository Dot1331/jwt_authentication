gen:
	protoc --proto_path=proto proto/*.proto  --go_out=:pb --go-grpc_out=:pb

server:
	go run cmd/server/main.go

user-login:
	 go run cmd/client/main.go  -user user1 -pass secret

admin-login:
	 go run cmd/client/main.go  -user admin1 -pass secret

wrong:
	 go run cmd/client/main.go  -user wrongusername -pass wrong