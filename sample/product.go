package sample

import (
	"jwt_autentication2/pb"
	"math/rand"
	"time"

	"github.com/google/uuid"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

// randomID returns random uuid
func randomID() string {
	return uuid.New().String()
}

// randomInt returns a random integer value
func randomInt(min, max int) int {
	return min + rand.Int()%(max-min+1)
}

// NewProduct returns a new sample Product
func NewProduct() *pb.Product {
	Product := &pb.Product{
		Id: randomID(),
	}

	return Product
}

// RandomProductScore returns a random Product score
func RandomProductScore() float64 {
	return float64(randomInt(1, 10))
}
